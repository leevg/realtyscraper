from django.conf.urls import patterns, url

from main.views import ProjectsListView, ScrapedView

urlpatterns = patterns('', 
	url(r'^$', ProjectsListView.as_view()),
	url(r'^scrape/', ScrapedView.as_view(), name='scrape'),
)
