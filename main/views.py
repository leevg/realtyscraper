# coding: utf-8
from django.conf import settings
from django.views.generic import ListView, TemplateView
from django.http import HttpResponseRedirect
from models import Project
from selenium import webdriver
from selenium.webdriver.common.by import By
import urllib2
from bs4 import BeautifulSoup


site = 'http://jm.se'


class ScrapedView(TemplateView):

    def get(self, request):

    	if settings.BROWSER == 'firefox':
    	    browser = webdriver.Firefox()
    	if settings.BROWSER == 'chrome':
    		browser = webdriver.Chrome()
    	if settings.BROWSER == 'safari':
    		browser = webdriver.Safari()    	
    	if settings.BROWSER == 'opera':
    		browser = webdriver.Opera()

        browser.get('http://www.jm.se/bostader/sok-bostad/#c=stockholm&tab=projects&listmode=box') 
        button = browser.find_element(By.ID,value="showmorebutton")
        
        while button.is_displayed():
            button.click()

        html_source = browser.page_source  
        browser.quit()
        soup = BeautifulSoup(html_source)
        container = soup.find(id='resultscontainer')
        
        for li in container.ul.find_all('li'):
            project_url = '{}{}'.format(site,li.a.get('href'))
            try:
                project = Project.objects.get(url=project_url)
            except Project.DoesNotExist:
             	project = Project()                	
            
            project.url = project_url
            project.picture_url = '{}{}'.format(site,li.img.get('src')[:-31])
            project.title = li.h2.string
            
            p = li.find_all('p')

            project.status = p[0].string
            project.area = p[2].string[8:]
            project.save()

        return HttpResponseRedirect('/')



class ProjectsListView(ListView): 
    model = Project                  