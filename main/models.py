# coding: utf-8
from django.db import models

# Create your models here.
class Project(models.Model):
	title = models.CharField(max_length=255)
	area = models.CharField(max_length=255)
	status = models.CharField(max_length=255)
	url = models.CharField(max_length=255)
	picture_url = models.CharField(max_length=255)